import 'dart:async';
import 'package:flutter/material.dart';


class CookingLogin extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => new CookingLoginState();

}

class CookingLoginState extends State<CookingLogin> {

  Future<String> _signIn() async {
    return 'signIn succeeded: UserName';
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new FlatButton(onPressed: () 
      {
        setState(() {
          _signIn();
        });
      }, 
      child: new Center(child: new Text("Login")),
      color: Colors.cyan,
      ),
      backgroundColor: Colors.blue,

    );
  }
}